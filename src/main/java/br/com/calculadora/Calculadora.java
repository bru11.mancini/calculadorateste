package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Calculadora {



    public Calculadora() {
    }

    public int soma(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public double soma(double primeiroNumero, double segundoNumero){
        Double resultado = primeiroNumero + segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);

        return bigDecimal.doubleValue();

        //tive que fazer o esquema do BigDecimal pra ele retornar apenas 5.7 e não 5.69999999
    }

    public int divisao(int  primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero / segundoNumero;
        return resultado;
    }

    public double divisao(double  primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero / segundoNumero;
        return resultado;
    }

    public int multiplicacao(int  primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    public double multiplicacao(double  primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero * segundoNumero;
        return resultado;
    }
}
