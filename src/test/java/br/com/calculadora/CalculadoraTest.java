package br.com.calculadora;

import com.sun.org.apache.xalan.internal.XalanConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp(){
        calculadora = new Calculadora();
    }


    @Test
    public void testaASomaDeDoisNumerosInteiros(){

        //Calculadora calculadora = new Calculadora(); retirei pq o beforeeach faz forçar a execução da função antes de cada método.


        int resultado = calculadora.soma(1,2);

        Assertions.assertEquals(3, resultado);

    }

    @Test
    public void testaASomaDeDoisNumerosFlutuantes(){
        //Calculadora calculadora = new Calculadora();
        double resultado = calculadora.soma(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaADivisaoPorNumeroInteiro(){
        int resultado = calculadora.divisao(10, 5);
        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void testaADivisaoPorNumeroFLutuante(){
        double resultado = calculadora.divisao(5, 2);
        Assertions.assertEquals(2.0, resultado);
    }

    @Test
    public void testaADivisaoPorNumeroNegativo(){
        double resultado = calculadora.divisao(-15, -3);
        Assertions.assertEquals(5, resultado);
    }

    @Test
    public void testaAMultiplicacaoPorNumeroInteiro(){
        int resultado = calculadora.multiplicacao(10, 5);
        Assertions.assertEquals(50, resultado);
    }

    @Test
    public void testaAMultiplicacaoPorNumeroFLutuante(){
        double resultado = calculadora.multiplicacao(2.5, 2.5);
        Assertions.assertEquals(6.25, resultado);
    }

    @Test
    public void testaAMultiplicacaoPorNumeroNegativo(){
        double resultado = calculadora.multiplicacao(-5, 3);
        Assertions.assertEquals(-15, resultado);
    }
}
